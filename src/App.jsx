import "./_app.scss";
import React from "react";
import Cards from "./components/cards/Cards";
import Table from "./components/table/Table";

function App() {
  return (
    <div id="screen">
      <div id="card-container">
        <Cards title="High Margin Recipes" />
        <Cards title="Low Margin Recipes" />
        <Cards title="Top Flactuating Recipes" />
      </div>
      <div id="table-container">
        <Table />
      </div>
    </div>
  );
}

export default App;
