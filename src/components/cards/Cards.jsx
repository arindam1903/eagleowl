import React from "react";
import "./_cards.scss";

export default function Cards(props) {
  const [highMargin, setHighMargin] = React.useState([]);
  const [lowMargin, setLowMargin] = React.useState([]);
  const [topFlactuating, setTopFlactuating] = React.useState([]);

  fetch(
    "https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/margin-group/?order=top&?limit=3"
  )
    .then((res) => res.json())
    .then((res) => setHighMargin(res.results));
  fetch(
    "https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/margin-group/?order=bottom&?limit=3"
  )
    .then((res) => res.json())
    .then((res) => setLowMargin(res.results));
  fetch(
    "https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/fluctuation-group/?order=top&?limit=3"
  )
    .then((res) => res.json())
    .then((res) => setTopFlactuating(res.results));

  return (
    <div id="card-main">
      <div id="card-title">{props.title}</div>
      <div id="card-description">
        {props.title === "High Margin Recipes"
          ? highMargin.map((el) => <div id="card-items">{el.name} </div>)
          : props.title === "Low Margin Recipes"
          ? lowMargin.map((el) => <div id="card-items">{el.name} </div>)
          : props.title === "Top Flactuating Recipes"
          ? topFlactuating.map((el) => <div id="card-items">{el.name} </div>)
          : null}
      </div>
    </div>
  );
}
