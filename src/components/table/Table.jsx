import React, { useEffect, Suspense } from "react";
import './_table.scss';
const TableBody = React.lazy(() => import("./TableBody"));

export default function Table() {
  const [table, setTable] = React.useState([]);
  const [isChecked, setISChecked] = React.useState(false);
  const[allrecipe,setAllRecipe]=React.useState(true);
  const[incorrect,setIncorrect]=React.useState(false);
  const[untagged,setUntagged]=React.useState(false);
  const[disabled,setDisabled]=React.useState(false);

  const [URL, setURL] = React.useState(
    "https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/recipes/?page=1"
  );

  const handleAllRecipe = () => {
    setURL(
      "https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/recipes/?page=1"
    );
    setIncorrect(false);
    setDisabled(false);
    setUntagged(false);
    setAllRecipe(true);
  };
  const handleIncorrect = () => {
    setURL(
      "https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/recipes/?page=1&?is_incorrect=true"
    );
    setAllRecipe(false);
    setDisabled(false);
    setUntagged(false);
    setIncorrect(true)
  };
  const handleUntagged = () => {
    setURL(
      "https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/recipes/?page=1&?is_untagged=true"
    );
    setIncorrect(false);
    setDisabled(false);
    setAllRecipe(false);
    setUntagged(true)
  };
  const handleDisabled = () => {
    setURL(
      "https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/recipes/?page=1&?is_disabled=true"
    );
    setIncorrect(false);
    setAllRecipe(false);
    setUntagged(false);
    setDisabled(true)
  };

  useEffect(() => {
    fetch(URL)
      .then((res) => res.json())
      .then((res) => {
        setTable(res.results);
      });
      setISChecked(false);
  }, [URL]);
  const handleCheck = () => {
    if (isChecked) setISChecked(false);
    else setISChecked(true);
  };

  return (
    <div id="table-main">
      <div id="navbar">
        <button onClick={handleAllRecipe} className={allrecipe?'nav-button selected-nav-button':'nav-button'} >ALL RECIPE(S)</button>
        <button onClick={handleIncorrect} className={incorrect?'nav-button selected-nav-button':'nav-button'}>INCORRECT</button>
        <button onClick={handleUntagged} className={untagged?'nav-button selected-nav-button':'nav-button'}>UNTAGGED</button>
        <button onClick={handleDisabled} className={disabled?'nav-button selected-nav-button':'nav-button'}>DISABLED</button>
      </div>

      <table>
        <thead>
          <tr>
            <th>
              <input type="checkbox" onClick={handleCheck} checked={isChecked?'checked':null} />
            </th>
            <th>NAME</th>
            <th>LAST UPDATED</th>
            <th>COGS%</th>
            <th>COST PRICE</th>
            <th>SALE PRICE</th>
            <th>GROSS MARGIN</th>
            <th>TAGS/ACTION</th>
          </tr>
        </thead>
        <tbody>
          <Suspense fallback={<div>Loading...</div>}>
            <TableBody table={table} isChecked={isChecked} />
          </Suspense>
        </tbody>
      </table>
    </div>
  );
}
