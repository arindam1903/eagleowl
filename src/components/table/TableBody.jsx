import React from "react";
import './_table.scss';

export default function TableBody(props) {
    
  let months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "June",
    "July",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  return (
    <>
      {props.table.map((el) => (
        <tr className='table-row'>
          <td>
            <input
              type="checkbox"
              checked={props.isChecked ? "checked" : null}
              onClick={props.isChecked ? "unchecked" : null}
            />
          </td>
          <td>{el.name}</td>
          <td>
            {months[new Date(el.last_updated.date).getMonth()] + " "}
            {new Date(el.last_updated.date).getDate() + " "}
            {new Date(el.last_updated.date).getFullYear()}
          </td>
          <td>{el.cogs}%</td>
          <td>{el.cost_price.toFixed(2)}</td>
          <td>{el.sale_price.toFixed(2)}</td>
          <td>{el.gross_margin.toFixed(2)}</td>
          <td ><button className='action-tag'>Indian</button><button className='action-tag'>Spicy</button></td>
        </tr>
      ))}
    </>
  );
}
